<?php

namespace interfaces;

use DTO\AddressDTO;

interface AddressInterface
{
    /**
     * @return AddressDTO
     */
    public function getAddress(): AddressDTO;
}