<?php

namespace DTO;

class AddressDTO
{
    public function __construct(
        public readonly string $contactName,
        public readonly string $phoneNumber,
        public readonly string $countryCode,
        public readonly string $state,
        public readonly string $city,
        public readonly string $address,
    )
    {
    }
}