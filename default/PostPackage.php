<?php

namespace default;

use DTO\AddressDTO;
use interfaces\AddressInterface;

class PostPackage implements AddressInterface
{
    /** @var int */
    public int $id;

    /** @var string */
    public string $contactName;

    /** @var string */
    public string $phoneNumber;

    /** @var string ISO 3166-1 alpha-2 */
    public string $countryName;

    /** @var string */
    public string $state;

    /** @var string */
    public string $city;

    /** @var string */
    public string $address;

    /** @var array */
    public array $packageItems;

    /**
     * @return AddressDTO
     */
    public function getAddress(): AddressDTO
    {
        return new AddressDTO(
            contactName: $this->contactName,
            phoneNumber: $this->phoneNumber,
            countryCode: $this->countryName,
            state: $this->state,
            city: $this->city,
            address: $this->address
        );
    }

}
