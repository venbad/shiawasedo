<?php

namespace default;

use DTO\AddressDTO;
use interfaces\AddressInterface;

class UserAddress implements AddressInterface
{
    /** @var int */
    public int $id;

    /** @var string */
    public string $name;

    /** @var string */
    public string $phoneNumber;

    /** @var string ISO 3166-1 alpha-2 */
    public string $countryCode;

    /** @var string */
    public string $state;

    /** @var string */
    public string $city;

    /** @var string */
    public string $street;

    /** @var string */
    public string $house;

    /** @var string */
    public string $apartmentNumber;

    /**
     * @return AddressDTO
     */
    public function getAddress(): AddressDTO
    {
        return new AddressDTO(
            contactName: $this->name,
            phoneNumber: $this->phoneNumber,
            countryCode: $this->countryCode,
            state: $this->state,
            city: $this->city,
            address: "{$this->street}, {$this->house}, {$this->apartmentNumber}"
        );
    }
}
