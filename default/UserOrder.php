<?php

namespace default;

use DTO\AddressDTO;
use interfaces\AddressInterface;

class UserOrder implements AddressInterface
{
    /** @var int */
    public int $id;

    /** @var string */
    public string $contactName;

    /** @var string */
    public string $phone;

    /** @var string ISO 3166-1 alpha-2 */
    public string $country;

    /** @var string */
    public string $state;

    /** @var string */
    public string $city;

    /** @var string */
    public string $street;

    /** @var string */
    public string $house;

    /** @var string */
    public string $apartment;

    /** @var array */
    public array $orderItems;

    /**
     * @return AddressDTO
     */
    public function getAddress(): AddressDTO
    {
        return new AddressDTO(
            contactName: $this->contactName,
            phoneNumber: $this->phone,
            countryCode: $this->country,
            state: $this->state,
            city: $this->city,
            address: "{$this->street}, {$this->house}, {$this->apartment}"
        );
    }
}
