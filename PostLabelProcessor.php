<?php

use DTO\AddressDTO;
use interfaces\AddressInterface;

class PostLabelProcessor
{
    /**
     * @param AddressInterface $someObjectWithAddress
     * @return string[]
     */
    public function getLabelData(AddressInterface $someObjectWithAddress): array
    {
        /**
         * @var AddressDTO
         */
        $addressData = $someObjectWithAddress->getAddress();

        return [
            'contactName' => $addressData->contactName,
            'phoneNumber' => $addressData->phoneNumber,
            'countryCode' => $addressData->countryCode,
            'state' => $addressData->state,
            'city' => $addressData->city,
            'address' => $addressData->address,
        ];
    }
}
